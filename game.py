from random import randint

# ask name
name = input("Hi! What is your name?")

for guess_num in range(1,6):
    # generate random month and year
    guess_year = randint(1924, 2004)
    guess_month = randint(1,12)
    print(f"Guess {guess_num}: {name} were you born in {guess_month}/ {guess_year} ?")

    response = input("yes or no?").lower()

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_num == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
